# GoGuess

A word guessing app using the fyne GUI framework. 
The game is fully functional and can play a 5-letter or a 6-letter word game. 
It can be run natively or compiled to a js/wasm webapp.

|                   5-letter game with dark theme                    |               6-letter game with light theme               |
|:------------------------------------------------------------------:|:----------------------------------------------------------:|
|      ![5 letter game - dark theme](app/assets/screenshot.png)      | ![6 letter game - light theme](app/assets/screenshot1.png) |

## Milestones
- [x] Playable game mechanics
- [x] Keyboard hints and key disabling
- [x] 5 or 6 word game
- [x] Preferences panel
- [x] Basic light/dark theme options
- [X] Test web compiled version
- [X] Save/Show Statistics
- [X] Save Preferences
- [ ] Test on other platforms

## Installation
- You will need to have [Go](https://go.dev/doc/install) v1.18 or later installed in order to build the application
- In order to package the application, you'll need the fyne command tools using 
```go install fyne.io/fyne/v2/cmd/fyne@latest```
- There is a make file to help with building, running, testing and packaging the application.

## License
[MIT](LICENSE)