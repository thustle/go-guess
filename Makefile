build:
	go build -o bin/go-guess ./app

run: build
	./bin/go-guess

test:
	go test -v ./...

package:
	fyne package --src app

package-web:
	fyne package -os web --src ./app
