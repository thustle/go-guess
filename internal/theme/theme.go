package theme

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
	"image/color"
)

type AppTheme struct {
	fyne.Theme
	variant fyne.ThemeVariant
}

var DarkTheme = newAppThemeWithVariant(theme.VariantDark)
var LightTheme = newAppThemeWithVariant(theme.VariantLight)

func newAppThemeWithVariant(variant fyne.ThemeVariant) fyne.Theme {
	return &AppTheme{Theme: theme.DefaultTheme(), variant: variant}
}

func (t *AppTheme) Color(name fyne.ThemeColorName, _ fyne.ThemeVariant) color.Color {
	return t.Theme.Color(name, t.variant)
}
