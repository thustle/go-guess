package data

import (
	"math/rand"
)

type WordSize int

const (
	WordSize5 WordSize = 5
	WordSize6 WordSize = 6
)

type Dictionary interface {
	GetRandomWord(size WordSize) string
	IsValid(word string) bool
}

type Words struct {
	wordsMap map[WordSize][]string
}

func NewWordsDictionary() *Words {
	return &Words{
		wordsMap: map[WordSize][]string{
			WordSize5: wordListFive,
			WordSize6: wordListSix,
		},
	}
}

func (w *Words) GetRandomWord(size WordSize) string {
	if size == WordSize6 {
		return wordListSix[rand.Intn(len(wordListSix))]
	}
	return wordListFive[rand.Intn(len(wordListFive))]
}

func (w *Words) IsValid(word string) bool {
	return Contains(w.wordsMap[WordSize(len(word))], word)
}

func Contains(list []string, word string) bool {
	for _, s := range list {
		if s == word {
			return true
		}
	}
	return false
}
