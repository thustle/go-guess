package data

import (
	"fyne.io/fyne/v2/test"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDictionaryWords(t *testing.T) {
	test.NewApp()
	dict := NewWordsDictionary()

	testCases := []struct {
		name       string
		wordLength WordSize
	}{
		{
			name:       "5 letter word",
			wordLength: WordSize5,
		},
		{
			name:       "6 letter word",
			wordLength: WordSize6,
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			word := dict.GetRandomWord(testCase.wordLength)
			assert.True(t, dict.IsValid(word))
			assert.False(t, dict.IsValid("XZXZXZ"[:int(testCase.wordLength)]))
		})
	}
}
