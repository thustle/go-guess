package errors

import "fmt"

type InvalidWordError struct {
	Attempted string
}

func (e InvalidWordError) Error() string {
	return fmt.Sprintf("%s is not a valid word", e.Attempted)
}
