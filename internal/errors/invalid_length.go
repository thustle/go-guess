package errors

import "fmt"

type InvalidLengthError struct {
	Wanted int
	Got    int
}

func (e InvalidLengthError) Error() string {
	return fmt.Sprintf("Word is only %d characters, %d needed", e.Got, e.Wanted)
}
