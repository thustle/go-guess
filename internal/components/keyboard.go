package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

type keyboard struct {
	widget.BaseWidget
	content           *fyne.Container
	keys              map[string]*key
	onTapped          func(key string)
	allowDisabledKeys bool
}

func withAllowingDisabledKeys(allowDisabledKeys bool) func(*keyboard) {
	return func(k *keyboard) {
		k.allowDisabledKeys = allowDisabledKeys
	}
}

func newKeyboard(onTapped func(key string), options ...func(*keyboard)) *keyboard {
	keyboard := &keyboard{
		onTapped: onTapped,
	}
	for _, o := range options {
		o(keyboard)
	}
	keyboard.ExtendBaseWidget(keyboard)
	keyboard.createContent()
	return keyboard
}

func (k *keyboard) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(k.content)
}

func (k *keyboard) MinSize() fyne.Size {
	return k.content.MinSize()
}

func (k *keyboard) createContent() {
	keyMap := k.createAlphaKeys()
	row1 := container.NewHBox(
		layout.NewSpacer(),
		keyMap["Q"],
		keyMap["W"],
		keyMap["E"],
		keyMap["R"],
		keyMap["T"],
		keyMap["Y"],
		keyMap["U"],
		keyMap["I"],
		keyMap["O"],
		keyMap["P"],
		layout.NewSpacer(),
	)
	row2 := container.NewHBox(
		layout.NewSpacer(),
		keyMap["A"],
		keyMap["S"],
		keyMap["D"],
		keyMap["F"],
		keyMap["G"],
		keyMap["H"],
		keyMap["J"],
		keyMap["K"],
		keyMap["L"],
		layout.NewSpacer(),
	)
	row3 := container.NewHBox(
		layout.NewSpacer(),
		k.createKeyWithLabel("Enter", " Enter "),
		keyMap["Z"],
		keyMap["X"],
		keyMap["C"],
		keyMap["V"],
		keyMap["B"],
		keyMap["N"],
		keyMap["M"],
		k.createKeyWithLabel("Delete", " Delete "),
		layout.NewSpacer(),
	)
	keyboard := container.NewVBox(
		separator(3),
		container.NewCenter(container.NewPadded(container.NewVBox(
			row1,
			row2,
			row3,
		))),
	)
	k.keys = keyMap
	k.content = keyboard
}

func (k *keyboard) createKey(key string) *key {
	return newKey(key, func() {
		k.onTapped(key)
	}, withAllowingDisabledKey(k.allowDisabledKeys))
}

func (k *keyboard) createKeyWithLabel(key string, label string) *key {
	return newKey(label, func() {
		k.onTapped(key)
	})
}

func (k *keyboard) createAlphaKeys() map[string]*key {
	keys := make(map[string]*key)
	for _, row := range []string{"QWERTYUIOP", "ASDFGHJKL", "ZXCVBNM"} {
		for _, char := range row {
			key := string(char)
			keys[key] = k.createKey(key)
		}
	}
	return keys
}

func (k *keyboard) setKeysState(result *guessCharResult) {
	for _, value := range result.noMatchChars {
		if key, ok := k.keys[value]; ok {
			key.SetState(keyStateDisabled)
		}
	}
	for _, value := range result.greenChars {
		if key, ok := k.keys[value]; ok {
			key.SetState(keyStateGreen)
		}
	}
	for _, value := range result.yellowChars {
		if key, ok := k.keys[value]; ok && key.state != keyStateGreen {
			key.SetState(keyStateYellow)
		}
	}
}

func (k *keyboard) clear() {
	for _, key := range k.keys {
		key.SetState(keyStateEnabled)
	}
}

func (k *keyboard) setAllowDisabledKeys(d bool) {
	k.allowDisabledKeys = d
	for _, key := range k.keys {
		key.allowDisabled = d
	}
}
