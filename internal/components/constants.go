package components

import "image/color"

var white = color.White
var darkGrey = color.RGBA{R: 135, G: 138, B: 140, A: 255}
var veryDarkGrey = color.RGBA{R: 65, G: 68, B: 70, A: 255}
var lightGrey = color.RGBA{R: 211, G: 215, B: 218, A: 255}
var yellow = color.RGBA{R: 215, G: 205, B: 59, A: 255}
var green = color.RGBA{R: 76, G: 174, B: 80, A: 255}
