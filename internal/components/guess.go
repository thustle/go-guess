package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"goguess/internal/data"
	"goguess/internal/errors"
	"strings"
)

type guess struct {
	widget.BaseWidget
	content      *fyne.Container
	dictionary   data.Dictionary
	size         int
	currentGuess string
	guessLetters []*guessLetter
}

func newGuess(size int, dictionary data.Dictionary) *guess {
	guess := &guess{
		size:         size,
		guessLetters: make([]*guessLetter, size),
		dictionary:   dictionary,
	}
	guess.createContent()
	return guess
}

func (g *guess) createContent() {
	row := container.New(layout.NewGridLayout(g.size))
	for i := 0; i < g.size; i++ {
		letter := newGuessLetter()
		g.guessLetters[i] = letter
		row.Add(letter)
	}
	g.content = row
	g.ExtendBaseWidget(g)
}

func (g *guess) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(g.content)
}

func (g *guess) MinSize() fyne.Size {
	return g.content.MinSize()
}

func (g *guess) setLetter(character string) {
	inputLetterIndex := len(g.currentGuess)
	if inputLetterIndex < g.size {
		g.guessLetters[inputLetterIndex].setCharacter(character)
		g.currentGuess = g.currentGuess + character
	}
}

func (g *guess) removeLetter() {
	inputLetterIndex := len(g.currentGuess) - 1
	if inputLetterIndex >= 0 {
		g.guessLetters[inputLetterIndex].setCharacter("")
		g.currentGuess = g.currentGuess[:inputLetterIndex]
	}
}

func (g *guess) clear() {
	g.currentGuess = ""
	for _, letter := range g.guessLetters {
		letter.clear()
	}
}

type checkResult struct {
	value  string
	status letterStatus
}

type masterResult struct {
	value     string
	allocated bool
}

func toCheckResults(guess string) []*checkResult {
	runes := []rune(guess)
	results := make([]*checkResult, len(runes))
	for i, r := range runes {
		results[i] = &checkResult{value: string(r)}
	}
	return results
}

func toMasterResult(wanted string) []*masterResult {
	runes := []rune(wanted)
	results := make([]*masterResult, len(runes))
	for i, r := range runes {
		results[i] = &masterResult{value: string(r)}
	}
	return results
}

func (g *guess) check(wanted string) (bool, *guessCharResult, error) {
	noMatchChars := make([]string, 0)
	yellowChars := make([]string, 0)
	greenChars := make([]string, 0)
	if len(wanted) != len(g.currentGuess) {
		return false, newCharResult(noMatchChars, yellowChars, greenChars), &errors.InvalidLengthError{Wanted: len(wanted), Got: len(g.currentGuess)}
	}
	if !g.dictionary.IsValid(g.currentGuess) {
		return false, newCharResult(noMatchChars, yellowChars, greenChars), &errors.InvalidWordError{Attempted: g.currentGuess}
	}

	wantedResults := toMasterResult(wanted)
	guessResults := toCheckResults(g.currentGuess)
	for i, guessChar := range guessResults {
		if guessChar.value == wantedResults[i].value {
			guessChar.status = letterStatusGreen
			wantedResults[i].allocated = true
			greenChars = append(greenChars, guessChar.value)
		} else if !strings.Contains(wanted, guessChar.value) {
			noMatchChars = append(noMatchChars, guessChar.value)
			guessChar.status = letterStatusNoMatch
		} else {
			guessChar.status = letterStatusNotChecked
		}
	}
	for _, guessChar := range guessResults {
		if guessChar.status != letterStatusNotChecked {
			continue
		}
		if strings.Contains(wanted, guessChar.value) && letterExistsAndNotAllocated(wantedResults, guessChar.value) {
			guessChar.status = letterStatusYellow
			yellowChars = append(yellowChars, guessChar.value)
		} else {
			guessChar.status = letterStatusNoMatch
		}
	}
	for i, guessChar := range guessResults {
		g.guessLetters[i].setStatus(guessChar.status)
	}

	match := false
	if wanted == g.currentGuess {
		match = true
	}
	return match, newCharResult(noMatchChars, yellowChars, greenChars), nil
}

func letterExistsAndNotAllocated(wantedResults []*masterResult, guessChar string) bool {
	for _, letter := range wantedResults {
		if letter.value == guessChar && !letter.allocated {
			letter.allocated = true
			return true
		}
	}
	return false
}

func newCharResult(noMatchChars, yellowChars, greenChars []string) *guessCharResult {
	return &guessCharResult{
		noMatchChars: noMatchChars,
		yellowChars:  yellowChars,
		greenChars:   greenChars,
	}
}

type guessCharResult struct {
	noMatchChars []string
	yellowChars  []string
	greenChars   []string
}
