package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"image/color"
	"time"
)

type letterStatus int

const (
	letterStatusNotChecked letterStatus = iota
	letterStatusNoMatch
	letterStatusYellow
	letterStatusGreen
)

var statusColours = map[letterStatus]color.Color{
	letterStatusNotChecked: darkGrey,
	letterStatusNoMatch:    lightGrey,
	letterStatusYellow:     yellow,
	letterStatusGreen:      green,
}

type guessLetter struct {
	widget.BaseWidget
	content   *fyne.Container
	bg        *canvas.Rectangle
	text      *canvas.Text
	character string
	status    letterStatus
	allocated bool
}

func newGuessLetter() *guessLetter {
	letter := &guessLetter{}
	letter.createContent()
	return letter
}

func (l *guessLetter) createContent() {
	box := canvas.NewRectangle(statusColours[l.status])
	box.SetMinSize(fyne.NewSize(62, 62))
	box.StrokeColor = color.Black
	box.StrokeWidth = 1

	text := canvas.NewText("", white)
	text.Alignment = fyne.TextAlignCenter
	text.TextSize = 32
	text.TextStyle.Bold = true
	content := container.New(layout.NewStackLayout(), box, text)

	l.content = content
	l.bg = box
	l.text = text
	l.ExtendBaseWidget(l)
}

func (l *guessLetter) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(l.content)
}

func (l *guessLetter) MinSize() fyne.Size {
	return l.content.MinSize()
}

func (l *guessLetter) setCharacter(character string) {
	l.character = character
	l.text.Text = character
	l.text.Refresh()
}

func (l *guessLetter) setStatus(status letterStatus) {
	l.status = status
	if status != letterStatusNotChecked {
		canvas.NewColorRGBAAnimation(l.bg.FillColor, statusColours[l.status], time.Millisecond*500, func(c color.Color) {
			l.bg.FillColor = c
			canvas.Refresh(l.bg)
		}).Start()
	} else {
		l.bg.FillColor = statusColours[l.status]
	}
	l.content.Refresh()
}

func (l *guessLetter) clear() {
	l.setStatus(letterStatusNotChecked)
	l.bg.FillColor = statusColours[l.status]
	l.setCharacter("")
}
