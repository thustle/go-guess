package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/test"
	"fyne.io/fyne/v2/theme"
	"github.com/stretchr/testify/assert"
	"image/color"
	"testing"
)

func TestKeyHighlight(t *testing.T) {
	test.NewApp()

	testCases := []struct {
		name                string
		allowDisabled       bool
		state               keyState
		hoveredOver         bool
		expectedBackground  color.Color
		expectedText        color.Color
		expectedTapResponse bool
	}{
		{
			name:                "Disabled no hover",
			allowDisabled:       false,
			state:               keyStateDisabled,
			hoveredOver:         false,
			expectedBackground:  veryDarkGrey,
			expectedText:        darkGrey,
			expectedTapResponse: false,
		},
		{
			name:                "Disabled no hover allow disabled",
			allowDisabled:       true,
			state:               keyStateDisabled,
			hoveredOver:         false,
			expectedBackground:  veryDarkGrey,
			expectedText:        darkGrey,
			expectedTapResponse: true,
		},
		{
			name:                "Disabled hover",
			allowDisabled:       false,
			state:               keyStateDisabled,
			hoveredOver:         true,
			expectedBackground:  veryDarkGrey,
			expectedText:        darkGrey,
			expectedTapResponse: false,
		},
		{
			name:                "Disabled hover allow disabled",
			allowDisabled:       true,
			state:               keyStateDisabled,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Enabled no hover",
			allowDisabled:       false,
			state:               keyStateEnabled,
			hoveredOver:         false,
			expectedBackground:  darkGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Enabled no hover allow disabled",
			allowDisabled:       true,
			state:               keyStateEnabled,
			hoveredOver:         false,
			expectedBackground:  darkGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Enabled hover",
			allowDisabled:       false,
			state:               keyStateEnabled,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Enabled hover allow disabled",
			allowDisabled:       true,
			state:               keyStateEnabled,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Yellow no hover",
			allowDisabled:       false,
			state:               keyStateYellow,
			hoveredOver:         false,
			expectedBackground:  yellow,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Yellow no hover allow disabled",
			allowDisabled:       true,
			state:               keyStateYellow,
			hoveredOver:         false,
			expectedBackground:  yellow,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Yellow hover",
			allowDisabled:       false,
			state:               keyStateYellow,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Yellow hover allow disabled",
			allowDisabled:       true,
			state:               keyStateYellow,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Green no hover",
			allowDisabled:       false,
			state:               keyStateGreen,
			hoveredOver:         false,
			expectedBackground:  green,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Green no hover allow disabled",
			allowDisabled:       true,
			state:               keyStateGreen,
			hoveredOver:         false,
			expectedBackground:  green,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Green hover",
			allowDisabled:       false,
			state:               keyStateGreen,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
		{
			name:                "Green hover allow disabled",
			allowDisabled:       true,
			state:               keyStateGreen,
			hoveredOver:         true,
			expectedBackground:  lightGrey,
			expectedText:        white,
			expectedTapResponse: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			options := []func(*key){withAllowingDisabledKey(testCase.allowDisabled)}
			tapped := false
			key := newKey("A", func() { tapped = true }, options...)
			assert.NotNil(t, key)

			key.SetState(testCase.state)
			if testCase.hoveredOver {
				key.MouseIn(&desktop.MouseEvent{})
			}
			key.Tapped(&fyne.PointEvent{})

			assert.Equal(t, testCase.expectedBackground, key.bg.FillColor)
			assert.Equal(t, testCase.expectedText, key.text.Color)
			assert.Equal(t, testCase.expectedTapResponse, tapped)

			if testCase.hoveredOver {
				key.MouseMoved(&desktop.MouseEvent{})
				key.MouseOut()
			}
		})
	}
}

func TestIconKey(t *testing.T) {
	test.NewApp()
	tapped := false
	key := newKeyWithIcon(theme.IconNameHelp, func() { tapped = true })
	assert.NotNil(t, key)

	key.Tapped(&fyne.PointEvent{})
	assert.True(t, tapped)
}
