package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/test"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRenderKeyboard(t *testing.T) {
	test.NewApp()
	keyboard := newKeyboard(func(string) {})
	assert.NotNil(t, keyboard)

	test.AssertObjectRendersToImage(t, "keyboard.png", keyboard)
}

func TestRenderDisableKeys(t *testing.T) {
	test.NewApp()
	keyTapped := ""
	keyboard := newKeyboard(func(key string) {
		keyTapped = key
	})
	assert.NotNil(t, keyboard)
	keyboard.setKeysState(&guessCharResult{noMatchChars: []string{"S", "T", "A", "K", "E"}})

	keyboardRows := keyboard.content.Objects[1].(*fyne.Container).Objects[0].(*fyne.Container).Objects[0].(*fyne.Container)
	topRow := keyboardRows.Objects[0].(*fyne.Container)

	qKey := topRow.Objects[1].(*key)
	qKey.Tapped(&fyne.PointEvent{})
	assert.Equal(t, "Q", keyTapped)

	keyTapped = ""
	eKey := topRow.Objects[3].(*key)
	eKey.Tapped(&fyne.PointEvent{})
	assert.Equal(t, "", keyTapped)

	test.AssertObjectRendersToImage(t, "keyboard_some_keys_disabled.png", keyboard)

	keyboard.clear()
	test.AssertObjectRendersToImage(t, "keyboard.png", keyboard)
}

func TestDisableKeysAllowed(t *testing.T) {
	test.NewApp()
	keyTapped := ""
	keyboard := newKeyboard(func(key string) { keyTapped = key }, withAllowingDisabledKeys(true))
	assert.NotNil(t, keyboard)
	keyboard.setKeysState(&guessCharResult{noMatchChars: []string{"S", "T", "A", "K", "E"}})

	keyboardRows := keyboard.content.Objects[1].(*fyne.Container).Objects[0].(*fyne.Container).Objects[0].(*fyne.Container)
	topRow := keyboardRows.Objects[0].(*fyne.Container)

	qKey := topRow.Objects[1].(*key)
	qKey.Tapped(&fyne.PointEvent{})
	assert.Equal(t, "Q", keyTapped)

	eKey := topRow.Objects[3].(*key)
	eKey.Tapped(&fyne.PointEvent{})
	assert.Equal(t, "E", keyTapped)
}

func TestSetKeysState(t *testing.T) {
	test.NewApp()
	keyboard := newKeyboard(func(string) {})
	assert.NotNil(t, keyboard)
	keyboard.setKeysState(&guessCharResult{
		noMatchChars: []string{"Q"},
		yellowChars:  []string{"W"},
		greenChars:   []string{"E"},
	})

	keyboardRows := keyboard.content.Objects[1].(*fyne.Container).Objects[0].(*fyne.Container).Objects[0].(*fyne.Container)
	topRow := keyboardRows.Objects[0].(*fyne.Container)

	qKey := topRow.Objects[1].(*key)
	assert.Equal(t, veryDarkGrey, qKey.bg.FillColor)

	wKey := topRow.Objects[2].(*key)
	assert.Equal(t, yellow, wKey.bg.FillColor)

	eKey := topRow.Objects[3].(*key)
	assert.Equal(t, green, eKey.bg.FillColor)
}

func TestKeyboardKeyTaps(t *testing.T) {
	test.NewApp()
	keyboard := newKeyboard(func(string) {})
	assert.NotNil(t, keyboard)

	keyboardRows := keyboard.content.Objects[1].(*fyne.Container).Objects[0].(*fyne.Container).Objects[0].(*fyne.Container)
	topRow := keyboardRows.Objects[0].(*fyne.Container)
	bottomRow := keyboardRows.Objects[2].(*fyne.Container)

	tests := []struct {
		name           string
		key            *key
		expectedLabel  string
		expectedSignal string
	}{
		{
			name:           "Q Key",
			key:            topRow.Objects[1].(*key),
			expectedLabel:  "Q",
			expectedSignal: "Q",
		}, {
			name:           "Enter Key",
			key:            bottomRow.Objects[1].(*key),
			expectedLabel:  " Enter ",
			expectedSignal: "Enter",
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			keyTapped := ""
			keyboard.onTapped = func(key string) {
				keyTapped = key
			}

			testCase.key.onTapped()

			assert.Equal(t, testCase.expectedLabel, testCase.key.text.Text)
			assert.Equal(t, testCase.expectedSignal, keyTapped)
		})
	}
}

func TestSetKeysAllowDisabled(t *testing.T) {
	test.NewApp()
	keyboard := newKeyboard(func(string) {})
	assert.NotNil(t, keyboard)
	keyboard.setAllowDisabledKeys(true)

	keyboardRows := keyboard.content.Objects[1].(*fyne.Container).Objects[0].(*fyne.Container).Objects[0].(*fyne.Container)
	topRow := keyboardRows.Objects[0].(*fyne.Container)

	qKey := topRow.Objects[1].(*key)
	assert.True(t, qKey.allowDisabled)

	eKey := topRow.Objects[3].(*key)
	assert.True(t, eKey.allowDisabled)
}
