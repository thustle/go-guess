package components

import (
	"fyne.io/fyne/v2/test"
	"github.com/stretchr/testify/assert"
	"goguess/internal/data"
	"testing"
)

func TestGuessIncorrectSize(t *testing.T) {
	test.NewApp()
	dictionary := data.NewWordsDictionary()
	guess := newGuess(5, dictionary)
	assert.NotNil(t, guess)
	guess.setLetter("A")
	guess.setLetter("B")
	guess.setLetter("C")
	guess.setLetter("D")

	match, i, err := guess.check("ABCDE")

	assert.NotNil(t, err)
	assert.False(t, match)
	assert.Equal(t, &guessCharResult{noMatchChars: []string{}, yellowChars: []string{}, greenChars: []string{}}, i)
}

func TestGuessInvalidWord(t *testing.T) {
	test.NewApp()
	dictionary := data.NewWordsDictionary()
	guess := newGuess(5, dictionary)
	assert.NotNil(t, guess)
	guess.setLetter("A")
	guess.setLetter("B")
	guess.setLetter("C")
	guess.setLetter("D")
	guess.setLetter("E")

	match, i, err := guess.check("ABCDE")

	assert.NotNil(t, err)
	assert.False(t, match)
	assert.Equal(t, &guessCharResult{noMatchChars: []string{}, yellowChars: []string{}, greenChars: []string{}}, i)
}

func TestGuessCorrectWord(t *testing.T) {
	test.NewApp()
	dictionary := data.NewWordsDictionary()
	guess := newGuess(5, dictionary)
	assert.NotNil(t, guess)
	guess.setLetter("S")
	guess.setLetter("P")
	guess.setLetter("A")
	guess.setLetter("C")
	guess.setLetter("E")

	match, i, err := guess.check("SPACE")

	assert.Nil(t, err)
	assert.True(t, match)
	assert.Equal(t, &guessCharResult{noMatchChars: []string{}, yellowChars: []string{}, greenChars: []string{"S", "P", "A", "C", "E"}}, i)
}

func TestGuessLetterStatuses(t *testing.T) {
	test.NewApp()
	dictionary := data.NewWordsDictionary()
	guess := newGuess(5, dictionary)
	assert.NotNil(t, guess)

	tests := []struct {
		name             string
		master           string
		input            string
		expectedStatuses []letterStatus
		expectedMisses   []string
		expectedYellow   []string
		expectedGreen    []string
	}{
		{
			name:             "WALTZ <- TATTY",
			master:           "WALTZ",
			input:            "TATTY",
			expectedStatuses: []letterStatus{letterStatusNoMatch, letterStatusGreen, letterStatusNoMatch, letterStatusGreen, letterStatusNoMatch},
			expectedMisses:   []string{"Y"},
			expectedYellow:   []string{},
			expectedGreen:    []string{"A", "T"},
		},
		{
			name:             "SASSY <- GLASS",
			master:           "SASSY",
			input:            "GLASS",
			expectedStatuses: []letterStatus{letterStatusNoMatch, letterStatusNoMatch, letterStatusYellow, letterStatusGreen, letterStatusYellow},
			expectedMisses:   []string{"G", "L"},
			expectedYellow:   []string{"A", "S"},
			expectedGreen:    []string{"S"},
		},
		{
			name:             "GLOSS <- SLACK",
			master:           "GLOSS",
			input:            "SLACK",
			expectedStatuses: []letterStatus{letterStatusYellow, letterStatusGreen, letterStatusNoMatch, letterStatusNoMatch, letterStatusNoMatch},
			expectedMisses:   []string{"A", "C", "K"},
			expectedYellow:   []string{"S"},
			expectedGreen:    []string{"L"},
		},
		{
			name:             "CROSS <- SORRY",
			master:           "CROSS",
			input:            "SORRY",
			expectedStatuses: []letterStatus{letterStatusYellow, letterStatusYellow, letterStatusYellow, letterStatusNoMatch, letterStatusNoMatch},
			expectedMisses:   []string{"Y"},
			expectedYellow:   []string{"S", "O", "R"},
			expectedGreen:    []string{},
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			guess.clear()
			for _, letter := range []rune(testCase.input) {
				guess.setLetter(string(letter))
			}
			check, i, err := guess.check(testCase.master)

			assert.Nil(t, err)
			assert.False(t, check)
			assert.Equal(t, testCase.expectedMisses, i.noMatchChars)
			assert.Equal(t, testCase.expectedYellow, i.yellowChars)
			assert.Equal(t, testCase.expectedGreen, i.greenChars)
			for i, guessLetter := range guess.guessLetters {
				assert.Equal(t, testCase.expectedStatuses[i], guessLetter.status)
			}
		})
	}
}

func TestGuessRender(t *testing.T) {
	test.NewApp()
	dictionary := data.NewWordsDictionary()
	guess := newGuess(5, dictionary)
	assert.NotNil(t, guess)
	guess.setLetter("S")
	guess.setLetter("T")
	guess.removeLetter()
	guess.setLetter("P")
	guess.setLetter("E")
	guess.setLetter("A")
	guess.setLetter("K")

	match, result, err := guess.check("SPARE")

	assert.Nil(t, err)
	assert.False(t, match)
	assert.Equal(t, &guessCharResult{noMatchChars: []string{"K"}, yellowChars: []string{"E", "A"}, greenChars: []string{"S", "P"}}, result)
	test.AssertObjectRendersToImage(t, "guess_speak_spare.png", guess)
}
