package components

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"goguess/internal/data"
	"goguess/internal/preferences"
	"goguess/internal/statistics"
)

type appState struct {
	started      bool
	wordLength   int
	wordToGuess  string
	guessesMade  int
	guesses      []*guess
	noMatchChars []string
}

type GameBoard struct {
	widget.BaseWidget
	icon         *fyne.StaticResource
	content      *fyne.Container
	guessTable   *fyne.Container
	keyboard     *keyboard
	appState     *appState
	prefs        *preferences.Preferences
	dictionary   data.Dictionary
	messagePanel *messagePanel
	setMessage   func(string)
	title        *canvas.Text
	OpenPrefs    func()
	OpenStats    func()
	stats        statistics.StatsUpdater
}

func WithIcon(icon *fyne.StaticResource) func(*GameBoard) {
	return func(g *GameBoard) {
		g.icon = icon
	}
}

func NewGameBoard(preferences *preferences.Preferences, dictionary data.Dictionary, stats statistics.StatsUpdater, options ...func(*GameBoard)) *GameBoard {
	appState := &appState{
		wordLength: int(preferences.WordSize),
	}
	game := &GameBoard{
		appState:   appState,
		prefs:      preferences,
		dictionary: dictionary,
		stats:      stats,
	}
	for _, o := range options {
		o(game)
	}
	preferences.OnChange = append(preferences.OnChange, game.OnPreferencesChanged)
	game.createContent()
	return game
}

func (g *GameBoard) createContent() {
	topBorder := g.newTopBorder()
	messages := g.createMessagePanel()
	topSection := container.NewVBox(
		topBorder,
		messages,
	)
	g.keyboard = newKeyboard(g.HandleCharacter)
	g.keyboard.allowDisabledKeys = g.prefs.AllowDisabledKeys
	board := container.NewBorder(topSection, g.keyboard, nil, nil)
	board.Add(g.newGuessTable())
	g.content = board
	g.ExtendBaseWidget(g)
}

func (g *GameBoard) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(g.content)
}

func (g *GameBoard) MinSize() fyne.Size {
	return g.content.MinSize()
}

func (g *GameBoard) newTopBorder() *fyne.Container {
	g.title = canvas.NewText("GoGuess", theme.ForegroundColor())
	g.title.TextSize = 28
	g.title.TextStyle.Bold = true
	content := container.NewHBox()
	if g.icon != nil {
		icon := canvas.NewImageFromResource(g.icon)
		icon.SetMinSize(fyne.NewSize(40, 40))
		content.Add(icon)
	}
	content.Add(g.title)
	settingsIcon := newTappableIcon(theme.DefaultTheme().Icon(theme.IconNameSettings), func() {
		g.OpenPrefs()
	})
	rightMargin := container.NewHBox(
		settingsIcon,
		widget.NewSeparator(),
	)
	infoIcon := newTappableIcon(theme.DefaultTheme().Icon(theme.IconNameInfo), func() {
		g.OpenStats()
	})
	leftMargin := container.NewHBox(
		widget.NewSeparator(),
		infoIcon,
	)
	topBorder := container.NewBorder(nil, separator(3), leftMargin, rightMargin,
		container.New(layout.NewCenterLayout(), content),
	)
	return topBorder
}

func separator(height float32) fyne.CanvasObject {
	separator := canvas.NewRectangle(lightGrey)
	separator.SetMinSize(fyne.NewSize(0, height))
	return separator
}

func (g *GameBoard) createMessagePanel() fyne.CanvasObject {
	g.messagePanel = newMessagePanel()
	g.setMessage = func(message string) {
		g.messagePanel.setMessage(message)
	}
	return g.messagePanel
}

func (g *GameBoard) newGuessTable() *fyne.Container {
	wordLength := g.appState.wordLength
	rowLayout := container.NewVBox()
	g.appState.guesses = make([]*guess, 6)
	for i := 0; i < 6; i++ {
		g.appState.guesses[i] = newGuess(wordLength, g.dictionary)
		rowLayout.Add(g.appState.guesses[i])
	}
	grid := container.New(layout.NewCenterLayout(), rowLayout)
	g.guessTable = grid
	return grid
}

func (g *GameBoard) HandleCharacter(key string) {
	if !g.appState.started || g.appState.guessesMade >= len(g.appState.guesses) {
		return
	}
	if !g.prefs.AllowDisabledKeys && data.Contains(g.appState.noMatchChars, key) {
		return
	}
	switch key {
	case "Enter":
		g.checkWord()
	case "Delete":
		g.appState.guesses[g.appState.guessesMade].removeLetter()
	default:
		g.appState.guesses[g.appState.guessesMade].setLetter(key)
	}
}

func (g *GameBoard) NewGame() {
	g.setMessage("New game started")
	g.appState.started = true
	g.appState.guessesMade = 0
	g.appState.wordToGuess = g.dictionary.GetRandomWord(g.prefs.WordSize)
	g.appState.noMatchChars = make([]string, 0)
	g.keyboard.clear()
	if g.appState.wordLength == int(g.prefs.WordSize) {
		for i := 0; i < len(g.appState.guesses); i++ {
			g.appState.guesses[i].clear()
		}
	} else {
		g.appState.wordLength = int(g.prefs.WordSize)
		g.content.Remove(g.guessTable)
		g.content.Add(g.newGuessTable())
	}
}

func (g *GameBoard) endGame(won bool, message string) {
	g.setMessage(message)
	g.appState.started = false
	g.stats.UpdateStats(won, g.appState.guessesMade, fyne.CurrentApp().Preferences())
}

func (g *GameBoard) QuitGame() {
	if g.appState.started {
		g.endGame(false, fmt.Sprintf("Game over. The answer was %s", g.appState.wordToGuess))
	}
}

func (g *GameBoard) checkWord() {
	match, keyStateResults, err := g.appState.guesses[g.appState.guessesMade].check(g.appState.wordToGuess)
	if err != nil {
		g.setMessage(err.Error())
		return
	}
	if match {
		g.endGame(true, "You got it!")
		g.keyboard.setKeysState(keyStateResults)
		g.appState.noMatchChars = append(g.appState.noMatchChars, keyStateResults.noMatchChars...)
	} else {
		g.appState.guessesMade++
		g.keyboard.setKeysState(keyStateResults)
		g.appState.noMatchChars = append(g.appState.noMatchChars, keyStateResults.noMatchChars...)
		if g.appState.guessesMade >= len(g.appState.guesses) {
			g.QuitGame()
			return
		}
	}
}

func (g *GameBoard) ThemeUpdate() {
	g.title.Color = theme.ForegroundColor()
	g.messagePanel.ThemeUpdate()
}

func (g *GameBoard) OnPreferencesChanged() {
	g.keyboard.setAllowDisabledKeys(g.prefs.AllowDisabledKeys)

	if int(g.prefs.WordSize) != g.appState.wordLength {
		if g.appState.guessesMade == 0 {
			g.NewGame()
		} else {
			g.setMessage(fmt.Sprintf("Length will be %d in next game", g.prefs.WordSize))
		}
	}
}

type tappableIcon struct {
	widget.Icon
	onTapped func()
}

func newTappableIcon(res fyne.Resource, f func()) *tappableIcon {
	icon := &tappableIcon{}
	icon.ExtendBaseWidget(icon)
	icon.SetResource(res)
	icon.onTapped = f
	return icon
}

func (t *tappableIcon) Tapped(_ *fyne.PointEvent) {
	if t.onTapped != nil {
		t.onTapped()
	}
}

func (t *tappableIcon) TappedSecondary(_ *fyne.PointEvent) {
}
