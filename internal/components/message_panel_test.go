package components

import (
	"fyne.io/fyne/v2/test"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestMessagePanelMessage(t *testing.T) {
	test.NewApp()
	messagePanel := newMessagePanel()
	assert.NotNil(t, messagePanel)

	messagePanel.setMessage("This is a test")

	test.AssertObjectRendersToImage(t, "message_panel.png", messagePanel)

	time.Sleep(2 * time.Second)
	test.AssertObjectRendersToImage(t, "message_panel_faded.png", messagePanel)
}
