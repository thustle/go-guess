package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"image/color"
)

type keyState int

const (
	keyStateEnabled keyState = iota
	keyStateDisabled
	keyStateGreen
	keyStateYellow
)

type key struct {
	widget.BaseWidget
	content       *fyne.Container
	bg            *canvas.Rectangle
	text          *canvas.Text
	icon          fyne.ThemeIconName
	onTapped      func()
	hovered       bool
	state         keyState
	allowDisabled bool
}

func withAllowingDisabledKey(allowDisabledKeys bool) func(*key) {
	return func(k *key) {
		k.allowDisabled = allowDisabledKeys
	}
}

func newKey(value string, onTapped func(), options ...func(*key)) *key {
	key := &key{
		text:     createKeyText(value),
		onTapped: onTapped,
	}
	for _, o := range options {
		o(key)
	}
	return createContent(key)
}

func newKeyWithIcon(iconName fyne.ThemeIconName, onTapped func()) *key {
	key := &key{
		icon:     iconName,
		onTapped: onTapped,
	}
	return createContent(key)
}

func (k *key) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(k.content)
}

func createContent(k *key) *key {
	k.ExtendBaseWidget(k)
	k.bg = createKeyBackground()
	k.content = container.NewPadded(container.New(
		layout.NewStackLayout(),
		k.bg,
	))
	if k.text != nil {
		k.content.Add(k.text)
	}
	if k.icon != "" {
		k.content.Add(createKeyIcon(k.icon))
	}
	return k
}

func createKeyText(text string) *canvas.Text {
	if text != "" {
		text := canvas.NewText(text, white)
		text.Alignment = fyne.TextAlignCenter
		text.TextSize = 30
		text.TextStyle.Bold = true
		return text
	} else {
		return nil
	}
}

func createKeyIcon(iconName fyne.ThemeIconName) *widget.Icon {
	if iconName == "" {
		return nil
	} else {
		return widget.NewIcon(theme.DefaultTheme().Icon(iconName))
	}
}

func createKeyBackground() *canvas.Rectangle {
	box := canvas.NewRectangle(darkGrey)
	box.CornerRadius = 6
	box.StrokeColor = color.Black
	box.StrokeWidth = 1
	box.SetMinSize(fyne.NewSize(35, 52))
	return box
}

func (k *key) refreshKey() {
	switch {
	case k.state == keyStateDisabled:
		switch {
		case k.allowDisabled && k.hovered:
			k.bg.FillColor = lightGrey
			k.text.Color = white
		default:
			k.bg.FillColor = veryDarkGrey
			k.text.Color = darkGrey
		}
	case k.state == keyStateYellow:
		switch {
		case k.hovered:
			k.bg.FillColor = lightGrey
			k.text.Color = white
		default:
			k.bg.FillColor = yellow
			k.text.Color = white
		}
	case k.state == keyStateGreen:
		switch {
		case k.hovered:
			k.bg.FillColor = lightGrey
			k.text.Color = white
		default:
			k.bg.FillColor = green
			k.text.Color = white
		}
	default:
		switch {
		case k.hovered:
			k.bg.FillColor = lightGrey
			k.text.Color = white
		default:
			k.bg.FillColor = darkGrey
			k.text.Color = white
		}
	}
	k.content.Refresh()
}

func (k *key) MinSize() fyne.Size {
	return k.content.MinSize()
}

// MouseIn is called when a desktop pointer enters the widget
func (k *key) MouseIn(*desktop.MouseEvent) {
	k.hovered = true
	k.refreshKey()
}

func (k *key) MouseMoved(*desktop.MouseEvent) {
}

func (k *key) MouseOut() {
	k.hovered = false
	k.refreshKey()
}

func (k *key) Tapped(*fyne.PointEvent) {
	if k.onTapped != nil && (k.state != keyStateDisabled || k.allowDisabled) {
		k.onTapped()
	}
}

func (k *key) SetState(state keyState) {
	k.state = state
	k.refreshKey()
}
