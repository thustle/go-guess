package components

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"image/color"
	"time"
)

type messagePanel struct {
	widget.BaseWidget
	content *fyne.Container
	label   *canvas.Text
	fadeOut *fyne.Animation
}

func newMessagePanel() *messagePanel {
	m := &messagePanel{}
	m.createContent()
	return m
}

func (m *messagePanel) createContent() {
	m.ExtendBaseWidget(m)
	m.label = canvas.NewText("", theme.ForegroundColor())
	m.label.TextStyle.Bold = true

	m.content = container.NewPadded(
		container.NewCenter(m.label),
	)
}

func (m *messagePanel) CreateRenderer() fyne.WidgetRenderer {
	return widget.NewSimpleRenderer(m.content)
}

func (m *messagePanel) MinSize() fyne.Size {
	return m.content.MinSize()
}

func (m *messagePanel) setMessage(message string) {
	if m.fadeOut != nil {
		m.fadeOut.Stop()
	}
	m.label.Color = theme.ForegroundColor()
	m.label.Text = message
	m.label.Refresh()
	anim := canvas.NewColorRGBAAnimation(theme.ForegroundColor(), theme.BackgroundColor(), time.Second*2, func(c color.Color) {
		m.label.Color = c
		canvas.Refresh(m.label)
	})
	m.fadeOut = anim
	go func() {
		time.Sleep(2 * time.Second)
		if anim != m.fadeOut {
			return
		}
		m.fadeOut.Start()
	}()
}

func (m *messagePanel) ThemeUpdate() {
	m.setMessage("")
	m.label.Color = theme.ForegroundColor()
}
