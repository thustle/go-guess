package preferences

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/test"
	"github.com/stretchr/testify/assert"
	"goguess/internal/data"
	"testing"
)

func TestPreferencesOK(t *testing.T) {
	a := test.NewApp()
	window := a.NewWindow("prefs")
	prefs := LoadPreferences(a.Preferences())
	onChangeCalled := false
	prefs.OnChange = append(prefs.OnChange, func() { onChangeCalled = true })

	theme := "Dark"
	ShowPreferences(window, func(s string) {
		theme = s
	}, prefs, a.Preferences())

	prefsPopup.themeRg.SetSelected("Light")
	prefsPopup.wordSizeRg.SetSelected("6")
	prefsPopup.allowDisabledCheck.SetChecked(true)
	prefsPopup.okButton.Tapped(&fyne.PointEvent{})

	assert.Equal(t, "Light", theme)
	assert.Equal(t, data.WordSize6, prefs.WordSize)
	assert.True(t, prefs.AllowDisabledKeys)
	assert.True(t, onChangeCalled)

	savedPrefs := LoadPreferences(a.Preferences())
	assert.Equal(t, data.WordSize6, savedPrefs.WordSize)
	assert.True(t, savedPrefs.AllowDisabledKeys)
	assert.Equal(t, "Light", savedPrefs.ThemeName)
}

func TestPreferencesCancel(t *testing.T) {
	a := test.NewApp()
	window := a.NewWindow("prefs")
	prefs := LoadPreferences(a.Preferences())
	onChangeCalled := false
	prefs.OnChange = append(prefs.OnChange, func() { onChangeCalled = true })

	ShowPreferences(window, func(s string) {}, prefs, a.Preferences())

	prefsPopup.themeRg.SetSelected("Light")
	prefsPopup.wordSizeRg.SetSelected("6")
	prefsPopup.allowDisabledCheck.SetChecked(true)
	prefsPopup.cancelButton.Tapped(&fyne.PointEvent{})

	assert.Equal(t, data.WordSize5, prefs.WordSize)
	assert.False(t, prefs.AllowDisabledKeys)
	assert.False(t, onChangeCalled)
}
