package preferences

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"goguess/internal/data"
	theme2 "goguess/internal/theme"
)

type Preferences struct {
	WordSize          data.WordSize
	AllowDisabledKeys bool
	ThemeName         string
	OnChange          []func()
}

func LoadPreferences(p fyne.Preferences) *Preferences {
	wordSize := data.WordSize5
	wordSizeInt := p.IntWithFallback("prefs.wordSize", 5)
	if wordSizeInt == 6 {
		wordSize = data.WordSize6
	}
	themeName := p.StringWithFallback("prefs.theme", "Dark")

	return &Preferences{
		WordSize:          wordSize,
		AllowDisabledKeys: p.BoolWithFallback("prefs.allowDisabled", false),
		ThemeName:         themeName,
	}
}

func (prefs *Preferences) Save(p fyne.Preferences) {
	p.SetInt("prefs.wordSize", int(prefs.WordSize))
	p.SetBool("prefs.allowDisabled", prefs.AllowDisabledKeys)
	p.SetString("prefs.theme", prefs.ThemeName)
}

func (p *Preferences) NotifyChanges() {
	for _, c := range p.OnChange {
		c()
	}
}

type PreferencesPopup struct {
	widget.BaseWidget
	content                *widget.PopUp
	wordSizeRg, themeRg    *widget.RadioGroup
	allowDisabledCheck     *widget.Check
	cancelButton, okButton *widget.Button
	preferences            *Preferences
}

var prefsPopup *PreferencesPopup

func ShowPreferences(w fyne.Window, changeTheme func(string), livePrefs *Preferences, p fyne.Preferences) {
	if prefsPopup != nil && prefsPopup.content.Visible() {
		return
	}
	if prefsPopup == nil {
		prefsPopup = &PreferencesPopup{}
	}
	prefsPopup.preferences = &Preferences{
		WordSize:          livePrefs.WordSize,
		AllowDisabledKeys: livePrefs.AllowDisabledKeys,
	}
	prefsPopup.wordSizeRg = widget.NewRadioGroup([]string{"5", "6"}, func(s string) {
		if s == "6" {
			prefsPopup.preferences.WordSize = data.WordSize6
		} else {
			prefsPopup.preferences.WordSize = data.WordSize5
		}
	})
	if livePrefs.WordSize == data.WordSize6 {
		prefsPopup.wordSizeRg.SetSelected("6")
	} else {
		prefsPopup.wordSizeRg.SetSelected("5")
	}

	liveTheme := fyne.CurrentApp().Settings().Theme()
	prefsPopup.themeRg = widget.NewRadioGroup([]string{"Dark", "Light"}, func(s string) {
		changeTheme(s)
	})
	if liveTheme == theme2.LightTheme {
		prefsPopup.themeRg.SetSelected("Light")
	} else {
		prefsPopup.themeRg.SetSelected("Dark")
	}

	prefsPopup.allowDisabledCheck = widget.NewCheck("Allow incorrect letters in subsequent guesses", func(b bool) {
		prefsPopup.preferences.AllowDisabledKeys = b
	})
	prefsPopup.allowDisabledCheck.SetChecked(livePrefs.AllowDisabledKeys)

	content := container.NewPadded(
		container.NewVBox(
			container.NewGridWithColumns(2,
				container.NewCenter(widget.NewLabel("Number of letter in word:")),
				prefsPopup.wordSizeRg,
				container.NewBorder(widget.NewSeparator(), nil, nil, nil, container.NewCenter(widget.NewLabel("Theme:"))),
				container.NewBorder(widget.NewSeparator(), nil, nil, nil, prefsPopup.themeRg),
			),
			widget.NewSeparator(),
			prefsPopup.allowDisabledCheck,
		),
	)
	prefsPopup.okButton = widget.NewButton("OK", func() {
		prefsPopup.content.Hide()
		livePrefs.WordSize = prefsPopup.preferences.WordSize
		livePrefs.AllowDisabledKeys = prefsPopup.preferences.AllowDisabledKeys
		livePrefs.ThemeName = prefsPopup.themeRg.Selected
		livePrefs.Save(p)
		livePrefs.NotifyChanges()
	})
	prefsPopup.okButton.Importance = widget.HighImportance
	prefsPopup.cancelButton = widget.NewButton("Cancel", func() {
		prefsPopup.content.Hide()
		if liveTheme == theme2.LightTheme && prefsPopup.themeRg.Selected == "Dark" {
			changeTheme("Light")
		} else if liveTheme == theme2.DarkTheme && prefsPopup.themeRg.Selected == "Light" {
			changeTheme("Dark")
		}
	})
	popUpContent := container.NewVBox(
		content,
		container.NewPadded(
			container.NewHBox(
				layout.NewSpacer(),
				prefsPopup.cancelButton,
				prefsPopup.okButton,
			),
		),
	)
	widget.NewModalPopUp(popUpContent, w.Canvas())
	prefsPopup.content = widget.NewModalPopUp(popUpContent, w.Canvas())
	prefsPopup.content.Show()
}
