package statistics

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/test"
	"fyne.io/fyne/v2/theme"
	"github.com/stretchr/testify/assert"
	theme2 "goguess/internal/theme"
	"testing"
)

func TestStatistics_PopupRender(t *testing.T) {
	a := test.NewApp()
	window := a.NewWindow("stats")
	window.Resize(fyne.NewSize(420, 380))
	theme2.DarkTheme = theme.DefaultTheme()
	stats := &Statistics{
		Played:        10,
		Won:           8,
		CurrentStreak: 2,
		MaxStreak:     5,
		Distribution:  [6]int{2, 0, 4, 2, 0, 0},
	}
	ShowStats(window, stats, theme.DefaultTheme())

	test.AssertObjectRendersToImage(t, "stats_popup.png", statsPopup.content)
}

func TestStatistics_PopupOK(t *testing.T) {
	a := test.NewApp()
	window := a.NewWindow("stats")
	stats := &Statistics{}

	ShowStats(window, stats, theme.DefaultTheme())

	statsPopup.okButton.Tapped(&fyne.PointEvent{})
	assert.True(t, statsPopup.content.Hidden)
}

func TestStatistics_UpdateStatsWon(t *testing.T) {
	a := test.NewApp()
	stats := &Statistics{
		Played:        5,
		Won:           3,
		CurrentStreak: 2,
		MaxStreak:     2,
		Distribution:  [6]int{1, 2, 3, 4, 5, 6},
	}
	stats.UpdateStats(true, 3, a.Preferences())

	assert.Equal(t, 4, stats.Won)
	assert.Equal(t, 6, stats.Played)
	assert.Equal(t, 3, stats.CurrentStreak)
	assert.Equal(t, 3, stats.MaxStreak)
	assert.Equal(t, [6]int{1, 2, 3, 5, 5, 6}, stats.Distribution)
}

func TestStatistics_UpdateStatsLost(t *testing.T) {
	a := test.NewApp()
	stats := &Statistics{
		Played:        5,
		Won:           3,
		CurrentStreak: 2,
		MaxStreak:     2,
		Distribution:  [6]int{1, 2, 3, 4, 5, 6},
	}
	stats.UpdateStats(false, 6, a.Preferences())

	assert.Equal(t, 3, stats.Won)
	assert.Equal(t, 6, stats.Played)
	assert.Equal(t, 0, stats.CurrentStreak)
	assert.Equal(t, 2, stats.MaxStreak)
	assert.Equal(t, [6]int{1, 2, 3, 4, 5, 6}, stats.Distribution)
}

func TestStatistics_SaveAndLoad(t *testing.T) {
	a := test.NewApp()
	stats := &Statistics{
		Played:        5,
		Won:           3,
		CurrentStreak: 2,
		MaxStreak:     2,
		Distribution:  [6]int{1, 2, 3, 4, 5, 6},
	}
	stats.UpdateStats(false, 6, a.Preferences())

	loadedStats := LoadStats(a.Preferences())
	assert.Equal(t, stats.Won, loadedStats.Won)
	assert.Equal(t, stats.Played, loadedStats.Played)
	assert.Equal(t, stats.CurrentStreak, loadedStats.CurrentStreak)
	assert.Equal(t, stats.MaxStreak, loadedStats.MaxStreak)
	assert.Equal(t, stats.Distribution, loadedStats.Distribution)
}
