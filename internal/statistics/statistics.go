package statistics

import (
	"bytes"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"github.com/wcharczuk/go-chart/v2"
	"github.com/wcharczuk/go-chart/v2/drawing"
	theme2 "goguess/internal/theme"
	"log"
	"math"
	"strconv"
)

type StatsUpdater interface {
	UpdateStats(won bool, guessesMade int, p fyne.Preferences)
}

type Statistics struct {
	Played        int
	Won           int
	CurrentStreak int
	MaxStreak     int
	Distribution  [6]int
}

type StatsPopup struct {
	widget.BaseWidget
	content  *widget.PopUp
	okButton *widget.Button
}

var statsPopup *StatsPopup

func ShowStats(w fyne.Window, stats *Statistics, theme fyne.Theme) {
	if statsPopup != nil && statsPopup.content.Visible() {
		return
	}
	if statsPopup == nil {
		statsPopup = &StatsPopup{}
	}

	barChart := generateBarChart(stats, theme)
	chartContent := container.NewCenter()
	if barChart != nil {
		chartContent.Add(barChart)
	} else {
		chartContent.Add(widget.NewLabel("No guess distribution to show yet"))
	}
	content := container.NewPadded(
		container.NewVBox(
			container.NewCenter(
				widget.NewLabel("Statistics"),
			),
			container.NewGridWithColumns(4,
				container.NewCenter(widget.NewLabel("Played")),
				container.NewCenter(widget.NewLabel("Win %")),
				container.NewCenter(widget.NewLabel("Streak")),
				container.NewCenter(widget.NewLabel("Max Streak")),
				container.NewCenter(widget.NewLabel(strconv.Itoa(stats.Played))),
				container.NewCenter(widget.NewLabel(strconv.Itoa(int(math.Round(float64(stats.Won)/float64(stats.Played))*100)))),
				container.NewCenter(widget.NewLabel(strconv.Itoa(stats.CurrentStreak))),
				container.NewCenter(widget.NewLabel(strconv.Itoa(stats.MaxStreak))),
			),
			chartContent,
		),
	)
	okButton := widget.NewButton("OK", func() {
		statsPopup.content.Hide()
	})
	okButton.Importance = widget.HighImportance
	popUpContent := container.NewVBox(
		content,
		container.NewPadded(
			container.NewHBox(
				layout.NewSpacer(),
				okButton,
			),
		),
	)
	widget.NewModalPopUp(popUpContent, w.Canvas())
	statsPopup.okButton = okButton
	statsPopup.content = widget.NewModalPopUp(popUpContent, w.Canvas())
	statsPopup.content.Show()
}

func generateBarChart(stats *Statistics, theme fyne.Theme) *canvas.Image {
	dataExists := false
	for i := range stats.Distribution {
		if stats.Distribution[i] > 0 {
			dataExists = true
			break
		}
	}
	if !dataExists {
		return nil
	}
	var palette chart.ColorPalette
	if theme2.DarkTheme == theme {
		palette = &darkColourPalette{}
	} else {
		palette = chart.DefaultColorPalette
	}
	barChart := chart.BarChart{
		Title:        "Guess Distribution",
		Height:       300,
		Width:        400,
		BarWidth:     25,
		ColorPalette: palette,
		Bars: []chart.Value{
			{Value: float64(stats.Distribution[0]), Label: "1"},
			{Value: float64(stats.Distribution[1]), Label: "2"},
			{Value: float64(stats.Distribution[2]), Label: "3"},
			{Value: float64(stats.Distribution[3]), Label: "4"},
			{Value: float64(stats.Distribution[4]), Label: "5"},
			{Value: float64(stats.Distribution[5]), Label: "6"},
		},
	}
	buf := new(bytes.Buffer)
	err := barChart.Render(chart.PNG, buf)
	if err != nil {
		log.Fatalf("failed to render bar chart: %v", err)
	}
	image := canvas.NewImageFromReader(buf, "barChart")
	image.FillMode = canvas.ImageFillOriginal
	return image
}

func LoadStats(p fyne.Preferences) *Statistics {
	distSlice := p.IntListWithFallback("dist", []int{0, 0, 0, 0, 0, 0})
	distArray := [6]int{}
	copy(distArray[:], distSlice[:])
	return &Statistics{
		Played:        p.IntWithFallback("played", 0),
		Won:           p.IntWithFallback("won", 0),
		CurrentStreak: p.IntWithFallback("streak", 0),
		MaxStreak:     p.IntWithFallback("maxStreak", 0),
		Distribution:  distArray,
	}
}

func (stats *Statistics) saveStats(p fyne.Preferences) {
	p.SetInt("played", stats.Played)
	p.SetInt("won", stats.Won)
	p.SetInt("streak", stats.CurrentStreak)
	p.SetInt("maxStreak", stats.MaxStreak)
	p.SetIntList("dist", stats.Distribution[:])
}

func (stats *Statistics) UpdateStats(won bool, guessesMade int, p fyne.Preferences) {
	stats.Played += 1
	if won {
		stats.Won += 1
		stats.CurrentStreak += 1
		stats.Distribution[guessesMade] += 1
	} else {
		stats.CurrentStreak = 0
	}
	if stats.CurrentStreak > stats.MaxStreak {
		stats.MaxStreak = stats.CurrentStreak
	}
	stats.saveStats(p)
}

type darkColourPalette struct{}

func (ap darkColourPalette) BackgroundColor() drawing.Color {
	return drawing.ColorFromAlphaMixedRGBA(24, 29, 37, 255)
}

func (ap darkColourPalette) BackgroundStrokeColor() drawing.Color {
	return drawing.ColorFromAlphaMixedRGBA(24, 29, 37, 255)
}

func (ap darkColourPalette) CanvasColor() drawing.Color {
	return drawing.ColorFromAlphaMixedRGBA(24, 29, 37, 255)
}

func (ap darkColourPalette) CanvasStrokeColor() drawing.Color {
	return drawing.ColorWhite
}

func (ap darkColourPalette) AxisStrokeColor() drawing.Color {
	return drawing.ColorWhite
}

func (ap darkColourPalette) TextColor() drawing.Color {
	return drawing.ColorWhite
}

func (ap darkColourPalette) GetSeriesColor(index int) drawing.Color {
	return chart.GetAlternateColor(index)
}
