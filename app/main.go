//go:generate fyne bundle -o bundled.go assets/Icon.png

package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"goguess/internal/components"
	"goguess/internal/data"
	"goguess/internal/preferences"
	"goguess/internal/statistics"
	"goguess/internal/theme"
)

func main() {
	a := app.NewWithID("app.goguess")
	prefs := preferences.LoadPreferences(a.Preferences())
	if prefs.ThemeName == "Light" {
		a.Settings().SetTheme(theme.LightTheme)
	} else {
		a.Settings().SetTheme(theme.DarkTheme)
	}
	window := a.NewWindow("GoGuess")
	dictionary := data.NewWordsDictionary()
	stats := statistics.LoadStats(a.Preferences())
	gameBoard := components.NewGameBoard(prefs, dictionary, stats, components.WithIcon(resourceIconPng))
	gameBoard.OpenPrefs = func() {
		preferences.ShowPreferences(window, changeTheme(a, gameBoard), prefs, a.Preferences())
	}
	gameBoard.OpenStats = func() {
		statistics.ShowStats(window, stats, a.Settings().Theme())
	}
	window.SetContent(container.NewBorder(nil, nil, nil, nil, gameBoard))
	window.Resize(fyne.NewSize(560, 720))
	window.SetCloseIntercept(func() {
		a.Quit()
	})
	window.SetMainMenu(createMainMenu(gameBoard, window, a, prefs, stats))
	directInputFromWindowToGame(window, gameBoard)
	gameBoard.NewGame()
	window.ShowAndRun()
}

func directInputFromWindowToGame(window fyne.Window, game *components.GameBoard) {
	window.Canvas().SetOnTypedKey(func(key *fyne.KeyEvent) {
		if letter, exists := keyMap[key.Name]; exists {
			game.HandleCharacter(letter)
		} else if key.Name == fyne.KeyBackspace {
			game.HandleCharacter("Delete")
		} else if key.Name == fyne.KeyReturn {
			game.HandleCharacter("Enter")
		}
	})
}

func createMainMenu(game *components.GameBoard, w fyne.Window, a fyne.App, prefs *preferences.Preferences, stats *statistics.Statistics) *fyne.MainMenu {
	newGame := fyne.NewMenuItem("New Game", func() { game.NewGame() })
	newGame.Shortcut = &desktop.CustomShortcut{KeyName: fyne.KeyN, Modifier: fyne.KeyModifierShortcutDefault}
	quitGame := fyne.NewMenuItem("End current game", func() { game.QuitGame() })
	preferenceItem := fyne.NewMenuItem("Preferences", func() {
		preferences.ShowPreferences(w, changeTheme(a, game), prefs, a.Preferences())
	})
	statsItem := fyne.NewMenuItem("Statistics", func() {
		statistics.ShowStats(w, stats, a.Settings().Theme())
	})
	preferenceItem.Shortcut = &desktop.CustomShortcut{KeyName: fyne.KeySlash, Modifier: fyne.KeyModifierShortcutDefault}
	file := fyne.NewMenu("File", newGame, quitGame, fyne.NewMenuItemSeparator(), preferenceItem, fyne.NewMenuItemSeparator(), statsItem)
	return fyne.NewMainMenu(file)
}

func changeTheme(a fyne.App, game *components.GameBoard) func(string) {
	return func(newTheme string) {
		switch newTheme {
		case "Light":
			a.Settings().SetTheme(theme.LightTheme)
		default:
			a.Settings().SetTheme(theme.DarkTheme)
		}
		game.ThemeUpdate()
	}
}
