package main

import "fyne.io/fyne/v2"

var keyMap = map[fyne.KeyName]string{
	fyne.KeyA: "A",
	fyne.KeyB: "B",
	fyne.KeyC: "C",
	fyne.KeyD: "D",
	fyne.KeyE: "E",
	fyne.KeyF: "F",
	fyne.KeyG: "G",
	fyne.KeyH: "H",
	fyne.KeyI: "I",
	fyne.KeyJ: "J",
	fyne.KeyK: "K",
	fyne.KeyL: "L",
	fyne.KeyM: "M",
	fyne.KeyN: "N",
	fyne.KeyO: "O",
	fyne.KeyP: "P",
	fyne.KeyQ: "Q",
	fyne.KeyR: "R",
	fyne.KeyS: "S",
	fyne.KeyT: "T",
	fyne.KeyU: "U",
	fyne.KeyV: "V",
	fyne.KeyW: "W",
	fyne.KeyX: "X",
	fyne.KeyY: "Y",
	fyne.KeyZ: "Z",
}
